import 'dart:io';

void main() {
  //part1();
  part2();
}

void part2() async {
  String content = await File('in.txt').readAsString();

  List<String> chars = [];

  for (int i = 0; i < content.length; i++) {
    chars.add(content[i]);
  }

  for (int i = 13; i < chars.length; i++) {
    List<String> section = chars.sublist(i - 13, i + 1);

    bool sectionContainsDuplicates = false;

    for (int j = 0; j < section.length; j++) {
      List<String> otherValuesInSection = new List<String>.from(section);
      otherValuesInSection.removeAt(j);
      if (otherValuesInSection.contains(section[j])) {
        sectionContainsDuplicates = true;
        break;
      }
    }

    if (!sectionContainsDuplicates) {
      print("First start of packet marker has been detected after ${i + 1} processed chars");
      return;
    }
  }
}

void part1() async {
  String content = await File('in.txt').readAsString();

  for (int i = 3; i < content.length; i++) {
    String w = content[i];
    String x = content[i - 1];
    String y = content[i - 2];
    String z = content[i - 3];

    if (w != x && w != y && w != z && x != y && x != z && y != z) {
      print("First start of packet marker has been detected after ${i + 1} processed chars");
      return;
    }
  }
}
