const fs = require("fs");
const readline = require("readline");

// This function could have been used for part 1 too. Should have done it right the first time...
async function findTopNElvesAndTheirCalories(n) {
    const fileStream = fs.createReadStream("in.txt");
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let elfCalories = [];
    let currentElfIndex = 0;

    for await (const line of rl) {
        if (line != "") {
            if (elfCalories[currentElfIndex] == undefined) elfCalories[currentElfIndex] = parseInt(line);
            else elfCalories[currentElfIndex] += parseInt(line);
        } else {
            currentElfIndex++;
        }
    }
    
    elfCalories = elfCalories.sort(function(a, b) {
        return b - a;
    });

    let totalCaloriesOfTopNElves = 0;
    for (let i = 0; i < n; i++) {
        totalCaloriesOfTopNElves += elfCalories[i];
    }

    console.log(`The top ${n} elves carry a total amount of ${totalCaloriesOfTopNElves} calories.`);

}

async function findElfWithHighestCalories() {
    const fileStream = fs.createReadStream("in.txt");
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let highestElfCalories = -1;
    let highestElfIndex = -1;
    let currentElfCalories = 0;
    let currentElfIndex = 0;

    for await (const line of rl) {
        if (line != "") {
            currentElfCalories += parseInt(line);
        } else {
            if (currentElfCalories > highestElfCalories) {
                highestElfCalories = currentElfCalories;
                highestElfIndex = currentElfIndex;
            }
            currentElfIndex++;
            currentElfCalories = 0;
        }
    }

    console.log(`Elf with the most calories is elf number ${highestElfIndex+1} with ${highestElfCalories} calories.`);

}

//findElfWithHighestCalories();
findTopNElvesAndTheirCalories(3);