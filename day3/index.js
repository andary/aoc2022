const fs = require("fs");
const readline = require("readline");

const prios = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

async function part2() {
    const fileStream = fs.createReadStream("in.txt");
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let rucksacks = [];

    for await (const line of rl) {
        rucksacks.push(line);
    }

    let badgeItems = [];

    for (let i = 2; i < rucksacks.length; i+=3) {
        let firstRucksackItems = rucksacks[i-2].split('');
        let secondRucksackItems = rucksacks[i-1].split('');
        let thirdRucksackItems = rucksacks[i].split('');

        for (let item of firstRucksackItems) {
            if (secondRucksackItems.includes(item) && thirdRucksackItems.includes(item)) {
                badgeItems.push(item);
                break;
            }
        }
    }

    let totalPrio = 0;

    for (let item of badgeItems) {
        let prio = prios.indexOf(item)+1;
        totalPrio += prio;
    }
    
    console.log(`Sum of item priorities is ${totalPrio}`);

}

async function part1() {
    const fileStream = fs.createReadStream("in.txt");
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let misplacedItems = [];

    for await (const line of rl) {
        
        const itemsInFirstHalf = line.substring(0, line.length / 2).split('');
        for (let item of line.substring(line.length / 2, line.length).split('')) {
            if (itemsInFirstHalf.includes(item)) {
                misplacedItems.push(item);
                // The duplicate item can occur multiple times, but it has to be the same
                // Return after first match
                break;
            }
        }

    }

    let totalPrio = 0;

    for (let item of misplacedItems) {
        let prio = prios.indexOf(item)+1;
        totalPrio += prio;
    }
    
    console.log(`Sum of item priorities is ${totalPrio}`);

}

part2();