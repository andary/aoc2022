package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	//part1()
	part2()
}

func part1() {
	lines, err := getLinesFromFile("in.txt")
	if err != nil {
		panic(err)
	}

	totalContainedAssignments := 0

	for i := 0; i < len(lines); i++ {

		firstElfLower, _ := strconv.Atoi(strings.Split(strings.Split(lines[i], ",")[0], "-")[0])
		firstElfUpper, _ := strconv.Atoi(strings.Split(strings.Split(lines[i], ",")[0], "-")[1])
		secondElfLower, _ := strconv.Atoi(strings.Split(strings.Split(lines[i], ",")[1], "-")[0])
		secondElfUpper, _ := strconv.Atoi(strings.Split(strings.Split(lines[i], ",")[1], "-")[1])

		if secondElfLower >= firstElfLower && secondElfUpper <= firstElfUpper {
			totalContainedAssignments++
		} else if firstElfLower >= secondElfLower && firstElfUpper <= secondElfUpper {
			totalContainedAssignments++
		}

	}

	fmt.Println("Amount of assignments that are contained by the other one in the pair:", totalContainedAssignments)
}

func part2() {
	lines, err := getLinesFromFile("in.txt")
	if err != nil {
		panic(err)
	}

	totalContainedAssignments := 0

	for i := 0; i < len(lines); i++ {

		firstElfLower, _ := strconv.Atoi(strings.Split(strings.Split(lines[i], ",")[0], "-")[0])
		firstElfUpper, _ := strconv.Atoi(strings.Split(strings.Split(lines[i], ",")[0], "-")[1])
		secondElfLower, _ := strconv.Atoi(strings.Split(strings.Split(lines[i], ",")[1], "-")[0])
		secondElfUpper, _ := strconv.Atoi(strings.Split(strings.Split(lines[i], ",")[1], "-")[1])

		if (firstElfLower >= secondElfLower && firstElfLower <= secondElfUpper) || (firstElfUpper <= secondElfUpper && firstElfUpper >= secondElfLower) {
			totalContainedAssignments++
		} else if (secondElfLower >= firstElfLower && secondElfLower <= firstElfUpper) || (secondElfUpper <= firstElfUpper && secondElfUpper >= firstElfLower) {
			totalContainedAssignments++
		}

	}

	fmt.Println("Amount of assignments that are contained by the other one in the pair:", totalContainedAssignments)
}

func getLinesFromFile(path string) ([]string, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer f.Close()
	lines := []string{}
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}
