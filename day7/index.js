const fs = require("fs");
const readline = require("readline");

async function part2() {
    const fileStream = fs.createReadStream("in.txt");
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let tree = {};
    let currentDir = "";

    let sumOfFileSizes = 0;

    for await (const line of rl) {
        
        if (line.startsWith("$")) {
            if (line.startsWith("$ cd")) {
                if (line == "$ cd /") {
                    currentDir = "";
                } else if (line == "$ cd ..") {
                    dirPath = currentDir.split("/");
                    dirPath.pop();
                    currentDir = dirPath.join("/");
                } else {
                    currentDir += (currentDir != "" ? "/" : "") + line.split(" ")[2];
                }
            }
        } else {
            if (!line.startsWith("dir")) {
                fileName = line.split(" ")[1];
                fileSize = line.split(" ")[0];

                sumOfFileSizes += parseInt(fileSize);

                let treeChunk = tree;
                if (currentDir != "") {
                    dirPath = currentDir.split("/");
                    for (const part of dirPath) {
                        if (!(part in treeChunk)) {
                            treeChunk[part] = {}
                        }
                        treeChunk = treeChunk[part];
                    }
                }
                treeChunk[fileName] = fileSize;
            }
        }

    }

    console.log("Tree:");
    console.log(JSON.stringify(tree));
    console.log("-----------")

    const totalDiskSpace = 70000000;
    const requiredDiskSpace = 30000000;
    let freeDiskSpace = 70000000 - sumOfFileSizes;
    let spaceToBeFreed = requiredDiskSpace - freeDiskSpace;
    console.log(`We need to free ${spaceToBeFreed}`);

    let deletionOptions = [];
    
    getSizeOfTreeChunk(tree);
    console.log("-----------")
    console.log(deletionOptions);
    console.log(`The smallest possible directory that could be deleted has total size of ${Math.min(...deletionOptions)}`)

    function getSizeOfTreeChunk(treeChunk) {
        let size = 0;
        for (const [key, value] of Object.entries(treeChunk)) {
            
            if (typeof value === 'object') {
                size += getSizeOfTreeChunk(value);
            } else {
                size += parseInt(value);
            }

        }

        if (size >= spaceToBeFreed) {
            deletionOptions.push(size);
        }
    
        return size;
    }
}

async function part1() {
    const fileStream = fs.createReadStream("in.txt");
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let tree = {};
    let currentDir = "";

    let totalSizeOfDirectoriesAtMost100000 = 0;

    for await (const line of rl) {
        
        if (line.startsWith("$")) {
            if (line.startsWith("$ cd")) {
                if (line == "$ cd /") {
                    currentDir = "";
                } else if (line == "$ cd ..") {
                    dirPath = currentDir.split("/");
                    dirPath.pop();
                    currentDir = dirPath.join("/");
                } else {
                    currentDir += (currentDir != "" ? "/" : "") + line.split(" ")[2];
                }
            }
        } else {
            if (!line.startsWith("dir")) {
                fileName = line.split(" ")[1];
                fileSize = line.split(" ")[0];

                let treeChunk = tree;
                if (currentDir != "") {
                    dirPath = currentDir.split("/");
                    for (const part of dirPath) {
                        if (!(part in treeChunk)) {
                            treeChunk[part] = {}
                        }
                        treeChunk = treeChunk[part];
                    }
                }
                treeChunk[fileName] = fileSize;
            }
        }

    }

    console.log("Tree:");
    console.log(JSON.stringify(tree));
    console.log("-----------")
    getSizeOfTreeChunk(tree);
    console.log("-----------")
    console.log(`Sum of size of directories with at most 100000 is ${totalSizeOfDirectoriesAtMost100000}`);

    function getSizeOfTreeChunk(treeChunk) {
        let size = 0;
        for (const [key, value] of Object.entries(treeChunk)) {
            
            if (typeof value === 'object') {
                size += getSizeOfTreeChunk(value);
            } else {
                size += parseInt(value);
            }

        }

        if (size < 100000) {
            console.log(`found directory with size of ${size}`);
            totalSizeOfDirectoriesAtMost100000 += size;
        }
    
        return size;
    }
}

part2();