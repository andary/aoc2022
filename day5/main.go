package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func part2() {
	lines, err := getLinesFromFile("in.txt")
	if err != nil {
		panic(err)
	}

	parsingCrates := true
	crateColumns := make(map[int][]string)

	for i := 0; i < len(lines); i++ {

		if parsingCrates {
			if lines[i] == "" {
				parsingCrates = false

				// crateColumns still has the crates in slices with their character index as keys
				// refine the array to make the first key '0' and so on
				crateColumnsRefined := make(map[int][]string)
				index := 1
				// this is dumb and only works because I know that there are less than 10 stacks of crates
				// but just iterating through the map does not result in the correct order
				for j := 0; j < 100; j++ {
					crateColumn, ok := crateColumns[j]
					if ok {
						crateColumnsRefined[index] = crateColumn
						index++
					}
				}
				crateColumns = crateColumnsRefined
				continue
			}

			// Read in all characters of line and check if they are '['. If yes, next one will be a crate
			// Could have just iterated with steps of 4 in hindsight...
			rowCharacters := strings.Split(lines[i], "")

			for j := 0; j < len(rowCharacters); j++ {
				if strings.HasPrefix(rowCharacters[j], "[") {
					crateColumn, ok := crateColumns[j+1]
					if !ok {
						crateColumn = []string{}
					}
					crateColumn = append(crateColumn, rowCharacters[j+1])
					crateColumns[j+1] = crateColumn
				}

			}

		} else {
			// Parse move instructions
			fmt.Println(crateColumns)
			fmt.Println(i + 1)

			instructions := strings.Split(lines[i], " ")
			quantity, _ := strconv.Atoi(instructions[1])
			from, _ := strconv.Atoi(instructions[3])
			to, _ := strconv.Atoi(instructions[5])

			fromCrateColumn := crateColumns[from]
			toCrateColumn := crateColumns[to]

			crateColumns[from] = append([]string{}, fromCrateColumn[quantity:]...)
			fmt.Println(crateColumns[from])

			crateColumns[to] = append(fromCrateColumn[:quantity], toCrateColumn...)

		}

	}

	out := ""

	for j := 0; j < 10; j++ {
		crateColumn, ok := crateColumns[j]
		if ok {
			topCrate := crateColumn[0]
			out += topCrate
		}
	}

	fmt.Println("The crates on top of the stacks are: ", out)
}

func part1() {
	lines, err := getLinesFromFile("in.txt")
	if err != nil {
		panic(err)
	}

	parsingCrates := true
	crateColumns := make(map[int][]string)

	for i := 0; i < len(lines); i++ {

		if parsingCrates {
			if lines[i] == "" {
				parsingCrates = false

				// crateColumns still has the crates in slices with their character index as keys
				// refine the array to make the first key '0' and so on
				crateColumnsRefined := make(map[int][]string)
				index := 1
				// this is dumb and only works because I know that there are less than 10 stacks of crates
				// but just iterating through the map does not result in the correct order
				for j := 0; j < 100; j++ {
					crateColumn, ok := crateColumns[j]
					if ok {
						crateColumnsRefined[index] = crateColumn
						index++
					}
				}
				crateColumns = crateColumnsRefined
				continue
			}

			// Read in all characters of line and check if they are '['. If yes, next one will be a crate
			// Could have just iterated with steps of 4 in hindsight...
			rowCharacters := strings.Split(lines[i], "")

			for j := 0; j < len(rowCharacters); j++ {
				if strings.HasPrefix(rowCharacters[j], "[") {
					crateColumn, ok := crateColumns[j+1]
					if !ok {
						crateColumn = []string{}
					}
					crateColumn = append(crateColumn, rowCharacters[j+1])
					crateColumns[j+1] = crateColumn
				}

			}

		} else {
			// Parse move instructions

			instructions := strings.Split(lines[i], " ")
			quantity, _ := strconv.Atoi(instructions[1])
			from, _ := strconv.Atoi(instructions[3])
			to, _ := strconv.Atoi(instructions[5])

			for j := 0; j < quantity; j++ {
				crate := crateColumns[from][0]

				// Append to to
				toCrateColumn := crateColumns[to]
				newToCrateColumn := append([]string{crate}, toCrateColumn...)
				crateColumns[to] = newToCrateColumn

				// Remove from from
				fromCrateColumn := crateColumns[from]
				newFromCrateColumn := fromCrateColumn[1:]
				crateColumns[from] = newFromCrateColumn
			}

		}

	}

	out := ""

	for j := 0; j < 10; j++ {
		crateColumn, ok := crateColumns[j]
		if ok {
			topCrate := crateColumn[0]
			out += topCrate
		}
	}

	//fmt.Println(crateColumns)

	fmt.Println("The crates on top of the stacks are: ", out)
}

func getLinesFromFile(path string) ([]string, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer f.Close()
	lines := []string{}
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}
