import 'dart:io';

void main() {
  part2();
}

void part2() async {
  List<String> grid = (await File('in.txt').readAsString()).split("\n");

  int gridHeight = grid.length;
  int gridWidth = grid[0].length;

  int highScore = 0;

  for (int y = 0; y < gridHeight; y++) {
    for (int x = 0; x < gridWidth; x++) {
      int treeHeight = int.parse(grid[y][x]);
      //print("current tree is $x, $y with height $treeHeight");

      int scenicScore = 1;

      for (int d = 0; d < 4; d++) {
        int step = 1;
        bool blocked = false;
        while (!blocked) {
          int nextTreeX = x;
          int nextTreeY = y;
          if (d == 0) {
            nextTreeX += step;
          } else if (d == 1) {
            nextTreeY += step;
          } else if (d == 2) {
            nextTreeX -= step;
          } else if (d == 3) {
            nextTreeY -= step;
          }

          if (nextTreeX < 0 || nextTreeX >= gridWidth || nextTreeY < 0 || nextTreeY >= gridHeight) {
            scenicScore *= (step - 1);
            blocked = true;
          } else {
            if (int.parse(grid[nextTreeY][nextTreeX]) >= treeHeight) {
              scenicScore *= step;
              blocked = true;
            } else {
              step++;
            }
          }
        }
      }

      if (scenicScore > highScore) {
        highScore = scenicScore;
      }
    }
  }

  print("highest scenic score:: $highScore");
}

void part1() async {
  List<String> grid = (await File('in.txt').readAsString()).split("\n");

  int gridHeight = grid.length;
  int gridWidth = grid[0].length;

  int visibleTrees = 0;

  for (int y = 0; y < gridHeight; y++) {
    for (int x = 0; x < gridWidth; x++) {
      int treeHeight = int.parse(grid[y][x]);
      print("current tree is $x, $y with height $treeHeight");

      bool treeVisible = false;

      if (y == 0 || y == gridHeight - 1 || x == 0 || x == gridWidth - 1) treeVisible = true;

      if (!treeVisible) {
        bool upVisible = true;
        for (var u = 1; u <= (gridHeight - (gridHeight - y)); u++) {
          print("compare up against tree ${x}, ${y - u} with height ${grid[y - u][x]}");
          if (treeHeight <= int.parse(grid[y - u][x])) {
            upVisible = false;
            break;
          }
        }
        treeVisible = upVisible;
      }

      if (!treeVisible) {
        bool downVisible = true;
        for (var d = 1; d < (gridHeight - y); d++) {
          print("compare down against tree ${x}, ${y + d} with height ${grid[y + d][x]}");
          if (treeHeight <= int.parse(grid[y + d][x])) {
            downVisible = false;
            break;
          }
        }
        treeVisible = downVisible;
      }

      if (!treeVisible) {
        bool leftVisible = true;
        for (var l = 1; l < (gridWidth - (gridWidth - x) + 1); l++) {
          print("compare left against tree ${x - l}, ${y} with height ${grid[y][x - l]}");
          if (treeHeight <= int.parse(grid[y][x - l])) {
            leftVisible = false;
            break;
          }
        }
        treeVisible = leftVisible;
      }

      if (!treeVisible) {
        bool rightVisible = true;
        for (var r = 1; r < (gridWidth - x); r++) {
          print("compare right against tree ${x + r}, ${y} with height ${grid[y][x + r]}");
          if (treeHeight <= int.parse(grid[y][x + r])) {
            rightVisible = false;
            break;
          }
        }
        treeVisible = rightVisible;
      }

      if (treeVisible) visibleTrees++;
      print("tree at $x, $y is visible: $treeVisible");
    }
  }

  print("amount of visible tress: $visibleTrees");
}
