const fs = require("fs");
const readline = require("readline");

async function part1() {
    const fileStream = fs.createReadStream("in.txt");
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

   let totalScore = 0;

    for await (const line of rl) {
        let roundScore = 0;
        let moves = line.split(" ");
        // Rock
        if (moves[0] == "A") {
            if (moves[1] == "X") roundScore += 4;
            else if (moves[1] == "Y") roundScore += 8;
            else if (moves[1] == "Z") roundScore += 3;
        // Paper
        } else if (moves[0] == "B") {
            if (moves[1] == "X") roundScore += 1;
            else if (moves[1] == "Y") roundScore += 5;
            else if (moves[1] == "Z") roundScore += 9;
        // Scissors
        } else if (moves[0] == "C") {
            if (moves[1] == "X") roundScore += 7;
            else if (moves[1] == "Y") roundScore += 2;
            else if (moves[1] == "Z") roundScore += 6;
        }

        totalScore += roundScore;
    }
    
    console.log(`Your total score would be ${totalScore}`);

}

async function part2() {
    const fileStream = fs.createReadStream("in.txt");
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let totalScore = 0;

    // Points for my moves
    // Rock +1
    // Paper +2
    // Scissor +3

    for await (const line of rl) {
        let roundScore = 0;
        let moves = line.split(" ");
        // Rock
        if (moves[0] == "A") {
            // Lose: Scissor
            if (moves[1] == "X") roundScore += 0 + 3;
            // Draw: Rock
            else if (moves[1] == "Y") roundScore += 3 + 1;
            // Win: Paper
            else if (moves[1] == "Z") roundScore += 6 + 2;
        // Paper
        } else if (moves[0] == "B") {
            // Lose: Rock
            if (moves[1] == "X") roundScore += 0 + 1;
            // Draw: Paper
            else if (moves[1] == "Y") roundScore += 3 + 2;
            // Win: Scissor
            else if (moves[1] == "Z") roundScore += 6 + 3;
        // Scissors
        } else if (moves[0] == "C") {
            // Lose: Paper
            if (moves[1] == "X") roundScore += 0 + 2;
            // Draw: Scissor
            else if (moves[1] == "Y") roundScore += 3 + 3;
            // Win: Rock
            else if (moves[1] == "Z") roundScore += 6 + 1;
        }

        totalScore += roundScore;
    }
    
    console.log(`Your total score would be ${totalScore}`);

}

part2();